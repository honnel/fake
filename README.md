# Faken bis es rult App

## Upload to badge

* connect badge via usb-c wire
* press upper left and lower right button to mount as mass storage device
* copy to the badge

### MacOS

```bash
cp -r fake /Volumes/CARD10/apps && sync
```

### Linux

```bash
cp -r fake /…/CARD10/apps && sync
```

happy fake :)
